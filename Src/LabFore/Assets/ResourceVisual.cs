using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameResourse;
using TMPro;

// ������� ��������� �� �����
public class ResourceVisual : MonoBehaviour
{
    private TextMeshProUGUI _text;

    public void UpdateText(int x)
    {
        _text.text = $"{x.ToString()}";
    }

    public void SetText(TextMeshProUGUI _textProf)
    {
        _text = _textProf;
    }
}
