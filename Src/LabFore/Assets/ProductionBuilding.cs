using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using GameResourse;

// ���� ������ �������� �� ������.
public class ProductionBuilding : MonoBehaviour
{
    private ProductionTime _productionTime;
    ResourceBank _resourceBank;
    Slider _slider;
    Button _myButton;
    GameResource _res;

    public void Add(ProductionTime _productionTimeParam, Slider _sliderParam, ResourceBank _resourceBankParam, GameResource ResParam, Button _buttone)
    {
        _productionTime = _productionTimeParam;
        _slider = _sliderParam;
        _slider.interactable = true;
        _resourceBank = _resourceBankParam;

        _myButton = _buttone;
        
        _slider.interactable = false;

        _res = ResParam;

        _productionTime.OnCorut(_myButton, _slider, this);
    }

    public void AddMainComponent()
    {
        _slider.interactable = false;

        _resourceBank.ChangeResource(_res, 1);
    }
}
