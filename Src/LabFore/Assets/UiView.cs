using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameResourse;
using UnityEngine;
using TMPro;

public class UiView : MonoBehaviour, IPointerClickHandler // ������������ ������� ����� ����� �� ������
{
    [SerializeField] private GameResource _typeResourse;
    [SerializeField] private Button _clike;
    [SerializeField] private TextMeshProUGUI _text; 
    [SerializeField] private Game _myGame; 
    [SerializeField] private ResourceVisual _resVisual;
    [SerializeField] private ProductionTime _productionTime;
    [SerializeField] private ResourceBank _resourceBank;
    [SerializeField] private Slider _mySlider;

    // ��� ��������� ���� ProductionBuilding.
    [SerializeField] private ProductionBuilding _produkBilde;

    private void Awake()
    {
        _resourceBank.SetAtr(_resVisual, _myGame);
    }
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        // ��� ��������� ���� ProductionBuilding � ������ _produkBilde.
        _resVisual.SetText(_text);
        
        _produkBilde.Add(_productionTime, _mySlider, _resourceBank, _typeResourse, _clike);
    }
}
