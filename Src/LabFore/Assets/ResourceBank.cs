using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameResourse;

// ����������� ������
public class ResourceBank : MonoBehaviour
{
    private ResourceVisual _resourceVisualr;
    private Game _game;
    public Dictionary<GameResource, int> _dictionary;

    public void SetAtr(ResourceVisual _resVisu, Game _myGame)
    {
        _resourceVisualr = _resVisu;
        _game = _myGame;
        _dictionary = new Dictionary<GameResource, int>
        {
            [GameResource.HUMANS] = _game._gameConfig.Humans,
            [GameResource.FOOD] = _game._gameConfig.Food,
            [GameResource.WOOD] = _game._gameConfig.Wood,
            [GameResource.STONE] = 0,
            [GameResource.GOLD] = 0
        };
    }
   
    public void ChangeResource(GameResource r, int v)
    {
        _dictionary[r] += v;
        Debug.Log("ChangeResource");
        _resourceVisualr.UpdateText(_dictionary[r]);
    }

    public int GetResource(GameResource r)
    {
        return _dictionary[r];
    }
}
