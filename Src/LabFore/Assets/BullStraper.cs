using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullStraper : MonoBehaviour
{
    public Game _game;
}

// �����
/*
public class Game
{
    // ����� - ������� ��� �������� ������.
    //public GameStateMachine _gameStateMachine;
    private IGameStateMachine _gameStateMachine;
}*/

public interface IGameStateMachine
{

    public void ChangeState(IEnumerable state);
}

public class GameStateMachine: IGameStateMachine
{
    // ���������� ���������� �� ��������� ��� ���������� �������.
    //private GameState _currente;
    private IEnterableState _currente;

    public void ChangeState(IEnumerable state)
    {
        // ������ ����� ��������� �� null, ����� �� �������� ������.
        _currente?.OnExit();
        //_currente = state;
        _currente?.OnEnter();
    }
}

// ���������� ������.
/*
public class GameState
{
    public virtual void OnEnter();
    public virtual void OnExit();
    public virtual void OnUpdate();
}*/

// ������� ������
public interface IEnterableState
{
    // interface - 
    void OnEnter();
    void OnExit();
}

public interface IUpdateState
{
    void OnUpdate();
}