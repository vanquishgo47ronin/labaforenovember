using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

// ���� ������ �������� �� slider
public class ProductionTime : MonoBehaviour
{
    private ProductionBuilding _productionBuilding;
    private Button _myButton;
    private Slider _mySlider;

    public void OnCorut(Button _butn, Slider _slide, ProductionBuilding _bilding)
    {
        _productionBuilding = _bilding;
        _myButton = _butn;
        _mySlider = _slide;
        StartCoroutine("OffButton");
    }
    private IEnumerator OffButton()
    {
        _myButton.interactable = false;
        
        _mySlider.interactable = true;

        _mySlider.value = 0;

        while (true)
        {
            _mySlider.value += 1;
            if (_mySlider.value == _mySlider.maxValue)
            {
                _myButton.interactable = true;
                _mySlider.value = 0;
                StopCoroutine("OffButton");
                _productionBuilding.AddMainComponent();
            }
            yield return new WaitForSeconds(0.1F);
        }
    }
}
