namespace GameResourse
{
    public enum GameResource
    {
        HUMANS,
        FOOD,
        WOOD,
        STONE,
        GOLD
    };
}
